<?php

include 'CountTable.php';


$c = new CountTable;

$c->increment("Ricardo");
$c->increment("Ricardo");
$c->increment("Ricardo");

if ($c->currentCount("Ricardo") !== 3) {
    echo "ERROR! The count should be 3", PHP_EOL;
}

$c = new CountTable;
$c->increment("Maria");
$c->increment("Bebeto", 2);
$c->increment("Bebeto", 5);
$c->increment("Maria", 2);

if ($c->currentCount("Maria") !== 3) {
    echo "ERROR! The count should be 3", PHP_EOL;
}

if ($c->currentCount("Bebeto") !== 7) {
    echo "ERROR! The count should be 7", PHP_EOL;
}

//if ($c->currentCount("Filomena") !== 0) {
//    echo "ERROR! The count should be 0", PHP_EOL;
//}

$largest = $c->largest();
if ($largest[0] !== "Bebeto") {
    echo "ERROR! The largest key is Bebeto", PHP_EOL;
}

if ($largest[1] !== 7) {
    echo "ERROR! The largest count is 7", PHP_EOL;
}

