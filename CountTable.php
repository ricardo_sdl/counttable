<?php


class CountTable implements Countable {
    
    private $keys = [];
    private $currentKey = 0;
    private $counts = [];
    
    
    public function __construct() {
        $this->counts = array_pad($this->counts, 64, 0);
    }
    
    public function increment($key, $value = 1) {
        if (!isset($this->keys[$key])) {
            $this->keys[$key] = &$this->counts[$this->currentKey];
        }
        $this->currentKey += $this->keys[$key] == 0;
        $this->keys[$key] += $value;
        
        if ($this->currentKey == count($this->counts)) {
            $this->expandCountTable();
        }
    }
    
    private function expandCountTable() {
        $this->counts = array_merge($this->counts, array_fill($this->currentKey, ceil($this->currentKey * 1.61803398874), 0));
        //$this->counts = array_pad($this->counts, ceil(count($this->counts) * 1.61803398874), 0);
    }
    
    public function currentCount($key) {
        return $this->keys[$key];
    }
    
    private function getCountKey($key) {
        
    }
    
    public function largest() {
        $maxCount = 0;
        $maxCountKey = null;
        $keys = array_keys($this->keys);
        foreach($this->counts as $i => $count) {
            if ($count > $maxCount) {
                $maxCount = $count;
                $maxCountKey = $i;
            }
        }
        
        return [$keys[$maxCountKey], $maxCount];
        
    }
    
    public function count () {
        return count($this->counts);
    }
    
    
}