<?php

ini_set('memory_limit', -1);

include 'CountTable.php';

const DELIM = "\t";
if (count($argv) < 3) {
    echo "synopsis: ", basename(__FILE__), " filename keyfield valuefield";
    exit(1);
}

$fileName = $argv[1];
$keyFieldIndex = $argv[2];
$valueFieldIndex = $argv[3];
$maxFieldIndex = max($keyFieldIndex, $valueFieldIndex);

$sumByKey = new CountTable;
$file = fopen($fileName, "rb");

if ($file === FALSE) {
    echo "Cannot open the file:", $fileName;
    exit(2);
}

//while (($row = fgets($file)) !== FALSE) {
while (($fields = fgetcsv($file, 0, DELIM)) !== FALSE) {
    if (count($fields) > $maxFieldIndex)
        $sumByKey->increment($fields[$keyFieldIndex], $fields[$valueFieldIndex]);
}

if (count($sumByKey) == 0) {
    echo "No entries";
    exit(0);
}
$largestValue = $sumByKey->largest();
echo "max_key:", $largestValue[0], " sum:", $largestValue[1];